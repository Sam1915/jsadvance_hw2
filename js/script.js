const books = [
     { 
       author: "Люсі Фолі",
       name: "Список запрошених",
       price: 70 
     },
     {
      author: "Сюзанна Кларк",
      name: "Джонатан Стрейндж і м-р Норрелл",
     }, 
     { 
       name: "Дизайн. Книга для недизайнерів.",
       price: 70
     }, 
     { 
       author: "Алан Мур",
       name: "Неономікон",
       price: 70
     }, 
     {
      author: "Террі Пратчетт",
      name: "Рухомі картинки",
      price: 40
     },
     {
      author: "Анґус Гайленд",
      name: "Коти в мистецтві",
     }
];

let root = document.getElementById('root');
let ul = document.createElement('ul');

function validateBook(book) {
     const requiredFields = ['author', 'name', 'price'];
     try {
          requiredFields.forEach(element => {
               if (!(element in book)) {
                    throw new Error(`Відсутня властивість: ${element}`);
               }
          });
          return true;
     } catch (error) {
          console.error(`Помилка валідації: ${error.message}`);
          return false;
     }
}

function renderBook(book) {
     const li = document.createElement('li');
     li.textContent = `${book.name} - ${book.author ? book.author + ', ' : ''} 
               ${book.price ? book.price + ' грн' : ''}`;
     return li;
}

books.forEach(book => {
     if (validateBook(book)) {
          ul.appendChild(renderBook(book));
     }
});

root.appendChild(ul);